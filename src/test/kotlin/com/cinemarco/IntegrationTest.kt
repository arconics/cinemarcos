package com.cinemarco

import com.cinemarco.domain.*
import java.time.temporal.ChronoUnit.MINUTES

class IntegrationTest : TestBase() {

    @Test
    fun `full end to end`() {
        given(
            RoomAdded(room_5(), listOf(seat_A1(), seat_A2(), seat_B4())),
            ScreeningPlanned(screening_1(), december_2nd_2020_at_12(), cinema_1(), room_5(), movie_1(), price_10EUR()),
            SeatReserved(screening_1(), seat_A1(), Antonio(), expiresInADay())
        )

        whenever(ReserveSeats(screening_1(), seat_A2(), Antonio()))

        whenQuery(
            Reservations(Antonio())
        )

        thenExpectedResponse(
            ReservationsInfo(
                listOf(
                    ReservationInfo("Cinema 1", "Screening 1", "Seat A1"),
                    ReservationInfo("Cinema 1", "Screening 1", "Seat A2")
                )
            )
        )
    }

    @Test
    fun `online reservation`() {
//        The user chooses seats, and gives the name of the person doing the reservation (name and surname).
//        The system gives back the total amount to pay and reservation expiration time.
        given(
            RoomAdded(room_5(), listOf(seat_A1(), seat_A2(), seat_B4())),
            ScreeningPlanned(screening_1(), december_2nd_2020_at_12(), cinema_1(), room_5(), movie_1(), price_10EUR()),
            ScreeningPlanned(screening_2(), december_3nd_2020_at_18(), cinema_1(), room_5(), movie_1(), price_10EUR())
        )

        whenQuery(
            ScreeningsOnDay(december_2nd_2020())
        )

        thenExpectedResponse(
            listOf(ScreeningInfo(screening_1(), "Movie 1", december_2nd_2020_at_12(), "10.0EUR"))
        )

        whenQuery(Screening(screening_1()))

        thenExpectedResponse(ScreeningDetailsInfo("Room 5", listOf("Seat A1", "Seat A2", "Seat B4")))

        whenever(ReserveSeats(screening_1(), seat_A1(), Antonio()))
        whenever(ReserveSeats(screening_1(), seat_A2(), Antonio()))

        whenQuery(Payment(screening_1(), Antonio()))

        thenExpectedResponse(
            PaymentInfo("20.0 EUR", ClockSource.now().plus(10, MINUTES))
        )
    }
}