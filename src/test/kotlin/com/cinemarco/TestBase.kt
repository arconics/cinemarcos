package com.cinemarco


import com.cinemarco.domain.*
import com.cinemarco.infrastructure.CommandHandler
import com.cinemarco.infrastructure.EventStore
import com.cinemarco.infrastructure.QueryHandler
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.core.test.TestCase
import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.collections.shouldNotContainAnyOf
import io.kotest.matchers.shouldBe
import java.time.*

abstract class TestBase : AnnotationSpec() {

    private var publishedEventsOffset: Int = 0
    private val UTC = ZoneId.of("UTC")
    private var queryResponse: Any? = null
    private lateinit var customerReservations: CustomerReservations
    private lateinit var screeningAvailable: ScreeningsAvailable
    private lateinit var screeningDetails: ScreeningDetails
    private lateinit var payments: Payments
    private lateinit var queryHandler: QueryHandler
    private val eventStore = EventStore()

    override fun beforeTest(testCase: TestCase) {
        withTime(december_2nd_2020_at_12().minusHours(1))

        eventStore.clear()
        customerReservations = CustomerReservations()
        screeningAvailable = ScreeningsAvailable()
        screeningDetails = ScreeningDetails()
        payments = Payments()
        queryHandler = QueryHandler(customerReservations, screeningAvailable, screeningDetails, payments){
            queryResponse = it
        }
        eventStore.subscribe(customerReservations::onEvent)
        eventStore.subscribe(screeningAvailable::onEvent)
        eventStore.subscribe(screeningDetails::onEvent)
        eventStore.subscribe(payments::onEvent)
    }

    protected fun given(vararg events: Any) {
        events.forEach(eventStore::publish)
    }

    protected fun whenever(command: Any) {
        publishedEventsOffset = eventStore.currentOffset()
        val handler = CommandHandler(eventStore.history(), eventStore::publish)
        handler.handle(command)
    }

    protected fun whenQuery(query: Any) {
        queryHandler.handle(query)
    }

    protected fun thenExpect(vararg expectedEvents: Any) {
        withClue("published events: ${publishedEvents()}") {
            publishedEvents() shouldContainAll expectedEvents.toList()
        }
    }

    protected fun thenExpectedResponse(expectedResponse: Any) {
        withClue("query response: $queryResponse") {
            queryResponse shouldBe expectedResponse
        }
    }

    protected fun thenExpectNo(vararg expectedEvents: Any) {
        withClue("published events: ${publishedEvents()}") {
            publishedEvents() shouldNotContainAnyOf expectedEvents.toList()
        }
    }

    private fun publishedEvents() = eventStore.history(publishedEventsOffset)

    protected fun withTime(timestamp: Instant) {
        ClockSource.clock = Clock.fixed(timestamp, UTC)
    }

    protected fun withTime(dateTime: LocalDateTime) {
        ClockSource.clock = Clock.fixed(dateTime.toInstant(ZoneOffset.UTC), UTC)
    }
}
