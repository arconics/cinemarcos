package com.cinemarco

import com.cinemarco.domain.*

class QueryHandlerTest : TestBase() {

    @Test
    fun `a user can see reserved seats`() {
        given(
            RoomAdded(room_5(), listOf(seat_A1(), seat_A2(), seat_B4())),
            ScreeningPlanned(screening_1(), december_2nd_2020_at_12(), cinema_1(), room_5(), movie_1(), price_10EUR()),
            SeatReserved(screening_1(), seat_A1(), Glen(), expiresInADay()),
            SeatReserved(screening_1(), seat_A2(), Glen(), expiresInADay()),
            SeatReserved(screening_1(), seat_B4(), Zdenko(), expiresInADay())
        )

        whenQuery(
            Reservations(Glen())
        )

        thenExpectedResponse(
            ReservationsInfo(
                listOf(
                    ReservationInfo("Cinema 1", "Screening 1", "Seat A1"),
                    ReservationInfo("Cinema 1", "Screening 1", "Seat A2")
                )
            )
        )
    }
}
