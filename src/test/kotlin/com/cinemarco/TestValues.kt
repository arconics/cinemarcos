package com.cinemarco

import com.cinemarco.domain.*
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.temporal.ChronoUnit.DAYS

fun december_2nd_2020_at_12() = LocalDateTime.of(2020, 12, 2, 12, 0)
fun december_3nd_2020_at_18() = LocalDateTime.of(2020, 12, 3, 18, 0)
fun december_1nd_2020() = LocalDate.of(2020, 12, 1)
fun december_2nd_2020() = LocalDate.of(2020, 12, 2)
fun Glen() = CustomerId("Glen")
fun Antonio() = CustomerId("Antonio")
fun Zdenko() = CustomerId("Zdenko")
fun Maciej() = CustomerId("Maciej")
fun cinema_1() = CinemaId("1")
fun room_5() = RoomId("5")
fun seat_A1() = SeatId("A1")
fun seat_A2() = SeatId("A2")
fun seat_B4() = SeatId("B4")
fun screening_1() = ScreeningId("1")
fun screening_2() = ScreeningId("2")
fun movie_1() = MovieId("1")
fun price_10EUR() = Price(10.0, "EUR")
fun expiresInADay() = ClockSource.now().plus(1, DAYS)

