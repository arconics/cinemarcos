package com.cinemarco

import com.cinemarco.domain.CancelReservationsWithoutBookings
import com.cinemarco.domain.ReserveSeats
import java.time.Instant.now
import java.time.temporal.ChronoUnit.MINUTES

class CommandHandlerTest : TestBase() {

    @Test
    fun `a free seat can be reserved`() {
        given(
            RoomAdded(room_5(), listOf(seat_A1(), seat_A2(), seat_B4())),
            ScreeningPlanned(screening_1(), december_2nd_2020_at_12(), cinema_1(), room_5(), movie_1(), price_10EUR()),
            SeatReserved(screening_1(), seat_A1(), Glen(), expiresInADay())
        )

        whenever(
            ReserveSeats(screening_1(), seat_A2(), Glen())
        )

        thenExpect(
            SeatReserved(screening_1(), seat_A2(), Glen(), expiresInADay())
        )
    }

    @Test
    fun `an already reserved seat cannot be reserved`() {
        given(
            RoomAdded(room_5(), listOf(seat_A1(), seat_A2(), seat_B4())),
            ScreeningPlanned(screening_1(), december_2nd_2020_at_12(), cinema_1(), room_5(), movie_1(), price_10EUR()),
            SeatReserved(screening_1(), seat_A1(), Antonio(), expiresInADay())
        )

        whenever(
            ReserveSeats(screening_1(), seat_A1(), Zdenko())
        )

        thenExpect(
            SeatCannotBeReserved(screening_1(), seat_A1())
        )
    }

    @Test
    fun `if no booking happens within 12 minutes, the reservation is cancelled`() {
        given(
            RoomAdded(room_5(), listOf(seat_A1(), seat_A2(), seat_B4())),
            ScreeningPlanned(screening_1(), december_2nd_2020_at_12(), cinema_1(), room_5(), movie_1(), price_10EUR()),
            SeatReserved(screening_1(), seat_A1(), Antonio(), expiresInADay())
        )

        withTime(now().plus(13, MINUTES))

        whenever(CancelReservationsWithoutBookings(screening_1()))

        thenExpect(ReservationCancelled(screening_1(), seat_A1()))
    }

    @Test
    fun `if a booking happens within 12 minutes, the reservation is not cancelled`() {
        given(
            RoomAdded(room_5(), listOf(seat_A1(), seat_A2(), seat_B4())),
            ScreeningPlanned(screening_1(), december_2nd_2020_at_12(), cinema_1(), room_5(), movie_1(), price_10EUR()),
            SeatReserved(screening_1(), seat_A1(), Maciej(), expiresInADay()),
            BookingMade(screening_1(), seat_A1(), Maciej())
        )

        withTime(now().plus(11, MINUTES))

        whenever(CancelReservationsWithoutBookings(screening_1()))

        thenExpectNo(ReservationCancelled(screening_1(), seat_A1()))
    }

    @Test
    fun `reservation is only possible up to 15 minutes before the screening`() {
        val screeningTime = december_2nd_2020_at_12()
        given(
            RoomAdded(room_5(), listOf(seat_A1(), seat_A2(), seat_B4())),
            ScreeningPlanned(screening_1(), screeningTime, cinema_1(), room_5(), movie_1(), price_10EUR())
        )

        withTime(screeningTime.minus(14, MINUTES))

        whenever(ReserveSeats(screening_1(), seat_A1(), Zdenko()))

        thenExpect(SeatCannotBeReserved(screening_1(), seat_A1()))
    }
}
