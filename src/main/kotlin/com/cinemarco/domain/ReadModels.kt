package com.cinemarco.domain

import com.cinemarco.RoomAdded
import com.cinemarco.ScreeningPlanned
import com.cinemarco.SeatReserved
import java.time.temporal.ChronoUnit

class CustomerReservations {
    val reservations = mutableMapOf<CustomerId, MutableList<ReservationInfo>>()
    private val cinemas = mutableMapOf<ScreeningId, String>()

    fun onEvent(e: Any) {
        when (e) {
            is SeatReserved -> {
                val cinemaInfo = cinemas[e.screening]!!
                val screeningInfo = "Screening ${e.screening.id}"
                val seatInfo = "Seat ${e.seat.id}"
                val newReservation = ReservationInfo(cinemaInfo, screeningInfo, seatInfo)
                reservations
                    .getOrPut(e.customer) { mutableListOf() }
                    .add(newReservation)
            }
            is ScreeningPlanned -> {
                cinemas[e.screening] = "Cinema ${e.cinema.id}"
            }
        }
    }
}

class ScreeningsAvailable {
    val screenings = mutableListOf<ScreeningInfo>()

    fun onEvent(e: Any) {
        when (e) {
            is ScreeningPlanned -> {
                val movieTitle = "Movie ${e.movie.id}"
                val screeningTime = e.screeningTime

                screenings.add(ScreeningInfo(e.screening, movieTitle, screeningTime, e.price.toString()))
            }
        }
    }
}

class ScreeningDetails {

    val allSeatsPerRoom = mutableMapOf<RoomId, List<SeatId>>()
    val rooms = mutableMapOf<ScreeningId, RoomId>()
    val seatsPerScreening = mutableMapOf<ScreeningId, MutableList<SeatId>>()

    fun onEvent(e: Any) {
        when (e) {
            is RoomAdded -> {
                allSeatsPerRoom[e.roomId] = e.seats
            }
            is ScreeningPlanned -> {
                seatsPerScreening[e.screening] = allSeatsPerRoom[e.roomId]!!.toMutableList()
                rooms[e.screening] = e.roomId
            }
        }
    }

    fun info(screening: ScreeningId): ScreeningDetailsInfo {
        val room = "Room ${rooms[screening]!!.id}"
        val seats = seatsPerScreening[screening]!!.map { "Seat ${it.id}" }
        return ScreeningDetailsInfo(room, seats)
    }
}

class Payments {
    val prices = mutableMapOf<ScreeningId, Price>()
    val payments = mutableMapOf<Pair<CustomerId, ScreeningId>, MutableList<Price>>()
    fun onEvent(e: Any) {
        when (e) {
            is ScreeningPlanned -> {
                prices[e.screening] = e.price
            }
            is SeatReserved -> {
               payments
                   .getOrPut(Pair(e.customer, e.screening)) { mutableListOf() }
                   .add(prices[e.screening]!!)
            }
        }
    }
    fun info(customer: CustomerId, screening: ScreeningId): PaymentInfo {
        val total = payments[Pair(customer, screening)]!!.sumByDouble { it.amount }
        //TODO: get rid of currency for now?
        //TODO: expiration time -> should this be a policy?
        return PaymentInfo("$total EUR", ClockSource.now().plus(10, ChronoUnit.MINUTES))
    }
}