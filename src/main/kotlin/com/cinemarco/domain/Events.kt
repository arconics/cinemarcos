package com.cinemarco

import com.cinemarco.domain.*
import java.time.Instant
import java.time.LocalDateTime

data class SeatReserved(
    val screening: ScreeningId,
    val seat: SeatId,
    val customer: CustomerId,
    val expirationTime: Instant,
    val createdAt: Instant = ClockSource.clock.instant())

data class SeatCannotBeReserved(
    val screening: ScreeningId,
    val seat: SeatId,
    val createdAt: Instant = ClockSource.clock.instant())

data class ScreeningPlanned(
    val screening: ScreeningId,
    val screeningTime: LocalDateTime,
    val cinema: CinemaId,
    val roomId: RoomId,
    val movie: MovieId,
    val price: Price,
    val createdAt: Instant = ClockSource.clock.instant())

data class ReservationCancelled(
    val screening: ScreeningId,
    val seat: SeatId,
    val createdAt: Instant = ClockSource.clock.instant())

data class BookingMade(
    val screening: ScreeningId,
    val seat: SeatId,
    val customer: CustomerId,
    val createdAt: Instant = ClockSource.clock.instant())

data class RoomAdded(
    val roomId: RoomId,
    val seats: List<SeatId>
)


