package com.cinemarco.domain

data class ReserveSeats(
    val screening: ScreeningId,
    val seat: SeatId,
    val customer: CustomerId
)
data class CancelReservationsWithoutBookings(
    val screening: ScreeningId
)