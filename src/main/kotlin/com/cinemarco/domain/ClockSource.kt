package com.cinemarco.domain

import java.time.Clock

object ClockSource {
    var clock: Clock = Clock.systemUTC()
    fun now() = clock.instant()
}