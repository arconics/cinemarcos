package com.cinemarco.domain

import java.time.Instant
import java.time.LocalDateTime

data class ReservationsInfo(val reservations: List<ReservationInfo>)
data class ReservationInfo(val cinema: String, val screening:String, val seat: String)
data class ScreeningInfo(val id: ScreeningId, val movieTitle: String, val screeningTime: LocalDateTime, val price: String)
data class ScreeningDetailsInfo(val room: String, val availableSeats:List<String>)
data class PaymentInfo(val total: String, val expirationTime: Instant)