package com.cinemarco.domain

import com.cinemarco.*
import java.time.Duration
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

class ScreeningReservations(
        private val state: ScreeningReservationsState,
        val publish: (Any) -> Unit) {

    fun reserveSeats(screeningId: ScreeningId, seatId: SeatId, customerId: CustomerId) {
        if (seatAlreadyReserved(screeningId, seatId) || reservationTooCloseToScreeningTime(screeningId))
            publish(SeatCannotBeReserved(screeningId, seatId))
        else
            publish(SeatReserved(screeningId, seatId, customerId, reservationExpiration()))
    }

    fun cancelReservationIfNoBookingMade(screening: ScreeningId) {
        state.reservationTimes[screening]?.forEach { (seat, reservationTime) ->
            if (isSeatBooked(screening, seat).not() && reservationExpired(reservationTime)) {
                publish(ReservationCancelled(screening, seat))
            }
        }
    }

    private fun reservationTooCloseToScreeningTime(screeningId: ScreeningId): Boolean {
        return Duration.between(ClockSource.now(), state.screeningTimes[screeningId]!!.toInstant(ZoneOffset.UTC)!!).toMinutes() < 15
    }

    private fun reservationExpiration() = ClockSource.now().plus(1, ChronoUnit.DAYS)

    private fun seatAlreadyReserved(screeningId: ScreeningId, seatId: SeatId) =
            state.reservedSeats[screeningId]?.contains(seatId) == true

    private fun reservationExpired(reservationTime: Instant) =
            Duration.between(reservationTime, ClockSource.now()) > Duration.ofMinutes(12)

    private fun isSeatBooked(
            screening: ScreeningId,
            seat: SeatId) = state.bookings.contains(Pair(screening, seat))
}

class ScreeningReservationsState(events: Iterable<Any>) {
    val reservedSeats = mutableMapOf<ScreeningId, MutableList<SeatId>>()
    val reservationTimes = mutableMapOf<ScreeningId, MutableMap<SeatId, Instant>>()
    val bookings = mutableSetOf<Pair<ScreeningId, SeatId>>()
    val screeningTimes = mutableMapOf<ScreeningId, LocalDateTime>()

    init {
        events.forEach(this::apply)
    }

    private fun apply(e: Any) {
        when (e) {
            is ScreeningPlanned -> {
                reservedSeats[e.screening] = mutableListOf()
                screeningTimes[e.screening] = e.screeningTime
                reservationTimes[e.screening] = mutableMapOf()
            }
            is SeatReserved -> {
                reservedSeats[e.screening]!!.add(e.seat)
                reservationTimes[e.screening]!![e.seat] = e.createdAt
            }
            is BookingMade -> bookings.add(Pair(e.screening, e.seat))
        }
    }

    override fun toString(): String {
        return "ScreeningReservationsState(reservedSeats=$reservedSeats, reservationTimes=$reservationTimes, bookings=$bookings)"
    }
}
