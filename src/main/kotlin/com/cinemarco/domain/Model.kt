package com.cinemarco.domain

import java.math.BigDecimal

//import java.util.*

//class Name
//class Birthday
//class Email
//class Address
//data class Customer(
//    val id: String,
//    val name: Name = Name(),
//    val dateOfBirth: Birthday = Birthday(),
//    val email: Email = Email(),
//    val address: Address = Address()
//)
//
//class Title(val language: Locale, val title: String, val subtitle: String)
//class TimeDuration
//class AgeRating
//enum class MovieType { _3D, _2D }
//data class Movie(val title: Title, val duration: TimeDuration, val ageRating: AgeRating)
//class Cinema
//class MovieScreening(val movie: Movie, val screenings: List<Screening>)
//class Screen(val cinema: Cinema, val seats: List<Seat>)
//class ScreeningTime
//data class Screening(val id: String, val time: ScreeningTime = ScreeningTime(), val screen: Screen?=null, val movie: Movie?=null, val movieType: MovieType = MovieType._2D)
//enum class TicketType { ADULT, STUDENT, CHILD, SENIOR }
//enum class SeatType { REGULAR, D_BOX }
//class SeatLocation
//class PriceValue
//class Price(val value: PriceValue, currency: Currency)
//data class Seat(val id: String, val location: SeatLocation = SeatLocation(), val type: SeatType = SeatType.REGULAR)
//data class Ticket(val price: Price, val ticketType: TicketType, val seat: Seat, val screening: Screening)
//data class Reservation(val price: Price, val tickets: List<Ticket>)

data class CustomerId(val id: String)
data class SeatId(val id: String)
data class CinemaId(val id: String)
data class ScreeningId(val id: String)
data class MovieId(val id: String)
data class RoomId(val id: String)
data class Price(val amount: Double, val currency: String) {
    override fun toString(): String {
        return "${amount}${currency}"
    }
}