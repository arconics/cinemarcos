package com.cinemarco.domain

import java.time.LocalDate

data class Reservations(val customer: CustomerId)
data class ScreeningsOnDay(val day: LocalDate)
data class Screening(val screening: ScreeningId)
data class Payment(val screening: ScreeningId, val customerId: CustomerId)