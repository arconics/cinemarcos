package com.cinemarco.infrastructure

class EventStore(
    private val events: MutableList<Any> = mutableListOf(),
    private val subscribers: MutableList<(Any) -> Unit> = mutableListOf()) {

    fun publish(e: Any) {
        events.add(e)
        subscribers.forEach {
            it(e)
        }
    }

    fun subscribe(onEvent: (Any) -> Unit) {
        subscribers.add(onEvent)
    }

    fun clear() {
        events.clear()
        subscribers.clear()
    }

    fun history(fromOffset: Int=0) = events.toList().subList(fromOffset, events.size)

    fun currentOffset() = events.size
}