package com.cinemarco.infrastructure

import com.cinemarco.domain.CancelReservationsWithoutBookings
import com.cinemarco.domain.ScreeningReservations
import com.cinemarco.domain.ReserveSeats
import com.cinemarco.domain.ScreeningReservationsState

class CommandHandler(private val history: List<Any>, private val publish: (Any) -> Unit) {

    fun handle(command: Any) {
        when (command) {
            is ReserveSeats -> {
                with(command) {
                    val state = ScreeningReservationsState(history)
                    val screeningReservations = ScreeningReservations(state, publish)
                    screeningReservations.reserveSeats(screening, seat, customer)
                }
            }
            is CancelReservationsWithoutBookings -> {
                with(command) {
                    val state = ScreeningReservationsState(history)
                    val screeningReservations = ScreeningReservations(state, publish)
                    screeningReservations.cancelReservationIfNoBookingMade(screening)
                }
            }
        }
    }
}