package com.cinemarco.infrastructure

import com.cinemarco.domain.*

class QueryHandler(
    private val customerReservations: CustomerReservations,
    private val screeningsAvailable: ScreeningsAvailable,
    private val screeningDetails: ScreeningDetails,
    private val payments: Payments,
    val response: (Any) -> Unit
) {

    fun handle(query: Any) {
        when (query) {
            is Reservations -> {
                response(ReservationsInfo(customerReservations.reservations[query.customer]!!))
            }
            is ScreeningsOnDay -> {
                response(screeningsAvailable.screenings.filter {
                    it.screeningTime.toLocalDate() == query.day
                })
            }
            is Screening -> {
                response(screeningDetails.info(query.screening))
            }
            is Payment -> {
                response(payments.info(query.customerId, query.screening))
            }
        }
    }
}